package com.webmobril.musicapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.text.TextUtils;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.webmobril.musicapp.databinding.ActivityDetailsBinding;

public class DetailsActivity extends AppCompatActivity {
    ActivityDetailsBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details);


        Glide.with(this).load(getIntent().getStringExtra("image")).apply(new RequestOptions().placeholder(R.drawable.ic_launcher_background).error(R.drawable.ic_launcher_background)).into(binding.songImage);
   binding.artistName.setText(getIntent().getStringExtra("artist_name"));
   binding.primaryGenreNameName.setText(getIntent().getStringExtra("trackName"));
   if(TextUtils.isEmpty(getIntent().getStringExtra("kind")))
        {
            binding.categoryName.setText("No Track");
        }
        else
        {
            binding.categoryName.setText(String.valueOf(getIntent().getStringExtra("kind")));
        }




    }
}