package com.webmobril.musicapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.webmobril.musicapp.DetailsActivity;
import com.webmobril.musicapp.R;
import com.webmobril.musicapp.model.ResultsItem;

import java.util.List;

public class MusicListAdapter extends RecyclerView.Adapter<MusicListAdapter.MyViewHolder>
{
    Context context;
    List<ResultsItem> musiclist;
    public MusicListAdapter(Context context, List musiclist)
    {
        this.context = context;
        this.musiclist=musiclist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_musiclist, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();
        ResultsItem musiclistitemmodel = musiclist.get(position);
        Glide.with(context).load(musiclistitemmodel.getArtworkUrl100()).apply(new RequestOptions().placeholder(R.drawable.ic_launcher_background).error(R.drawable.ic_launcher_background)).into(holder.song_image);
        holder.artist_name.setText(musiclistitemmodel.getArtistName().trim());
        if(TextUtils.isEmpty(musiclistitemmodel.getTrackCensoredName()))
        {
            holder.trackName.setText("No Track");
        }
        else
        {
            holder.trackName.setText(String.valueOf(musiclistitemmodel.getTrackCensoredName()));
        }
        holder.itemView.setOnClickListener(view ->
        {
            Intent i=new Intent (context, DetailsActivity.class);
            i.putExtra("image",musiclist.get(position).getArtworkUrl100());
            i.putExtra("artist_name",musiclist.get(position).getArtistName().trim());
            i.putExtra("trackName",holder.trackName.getText().toString().trim());
            i.putExtra("kind",musiclist.get(position).getKind());
            context.startActivity(i);
        });
    }
    @Override
    public int getItemCount()
    {
        return musiclist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView artist_name,trackName,rating_count;
        ImageView song_image;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            song_image =itemView.findViewById(R.id.song_image);
            artist_name=itemView.findViewById(R.id.artist_name);
            trackName=itemView.findViewById(R.id.trackName);
        }
    }
}