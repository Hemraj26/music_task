package com.webmobril.musicapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.musicapp.adapter.MusicListAdapter;
import com.webmobril.musicapp.model.MusicListResponse;
import com.webmobril.musicapp.model.ResultsItem;
import com.webmobril.musicapp.network.ApiInterface;
import com.webmobril.musicapp.utils.ProgressD;
import com.webmobril.musicapp.utils.UrlApi;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MusicListActivity extends AppCompatActivity
{
    List<ResultsItem> musiclist;
    RecyclerView recyclerview;
    MusicListAdapter musiclistAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        getMusicList();
    }

    public void init()
    {
        recyclerview = findViewById(R.id.recyclerview);
    }

    private void getMusicList()
    {
        final ProgressD progressDialog = ProgressD.show(this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<MusicListResponse> call = service.MusicList();
        call.enqueue(new Callback<MusicListResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<MusicListResponse> call, retrofit2.Response<MusicListResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    MusicListResponse resultFile = response.body();

                         musiclist= resultFile.getResults();
                        recyclerview.setLayoutManager(new LinearLayoutManager(MusicListActivity.this, LinearLayoutManager.VERTICAL, false));
                        musiclistAdapter = new MusicListAdapter(MusicListActivity.this,musiclist);
                        recyclerview.setAdapter(musiclistAdapter);
                       // Toast.makeText(MusicListResponse.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<MusicListResponse> call, Throwable t)
            {
                Toast.makeText(MusicListActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    }