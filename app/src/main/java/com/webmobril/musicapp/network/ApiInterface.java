package com.webmobril.musicapp.network;



import com.webmobril.musicapp.model.MusicListResponse;
import com.webmobril.musicapp.utils.UrlApi;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface
{

    @GET(UrlApi.MusicList)
    Call<MusicListResponse> MusicList();

}
